#include <functional>
#include <iostream>
#include <memory>
#include "catch2/catch.hpp"
#include "MyHeap.hpp"

constexpr size_t SIZE = 1000000;
auto heap = Heap<int, int>(SIZE);
int	 prevKey 	 = 0;
int  prevValue = 100000;

TEST_CASE("Testing insertions", "[insertions]")
{
	int key = GENERATE(take(SIZE, random(-10000, 10000)));
	// int value = GENERATE(take(5, random(-100, 100)));

	heap.insert(std::make_pair(key, key));
	if (key < prevValue) {
		REQUIRE(heap.top() == key);
		prevKey 	= key;
		prevValue = key;
	} else {
		REQUIRE(heap.top() == prevKey);
	}
}