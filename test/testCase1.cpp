#include <functional>
#include <iostream>
#include <memory>
#include "catch2/catch.hpp"
#include "MyHeap.hpp"

auto heap = std::make_unique<Heap<int, int>>();


TEST_CASE("Check heap size", "[Heap]")
{
  SECTION("Empty heap")
  {
    REQUIRE(heap->isEmpty());
  }
  
  SECTION("Insert item")
  {
    heap->insert(std::make_pair(30, 1));
    REQUIRE(!heap->isEmpty());
    
    SECTION("Check top item")
    {
      REQUIRE(heap->top() == 30);
    }
  }  
}