#ifndef MYHEAP_HPP
#define MYHEAP_HPP

#include <vector>
#include <unordered_map>
#include <functional>
#include <utility>
#include <iostream>
#include <exception>
#include <iterator>

template<class Key, class Value, class Compare = std::less<Value>>
class Heap
{
	public:
		constexpr Heap() = default;
		constexpr Heap(size_t const& size);
		~Heap() = default;

		[[nodiscard]] constexpr auto isEmpty() const noexcept -> bool;
		[[nodiscard]] constexpr auto top() 		 const 					-> Key;
		[[nodiscard]] constexpr auto popTop() 								-> Key;
									constexpr void insert(std::pair<Key, Value> const& node);
									constexpr void insert(std::pair<Key, Value>&& 		 node);
									constexpr void resetValue(Key const& key, Value const& value);

  private:
									constexpr void insertLogic(Key const& key);
		[[nodiscard]] constexpr auto left(size_t const& parent)	 -> size_t;
		[[nodiscard]] constexpr auto right(size_t const& parent) -> size_t;
		[[nodiscard]] constexpr auto parent(size_t const& child) -> size_t;

									constexpr void heapifyUp(size_t const& index);
									constexpr void heapifyDown(size_t const& index);
									constexpr void swapKeys(size_t const a, size_t const b);

		std::unordered_map<const Key*, size_t> indexes{};
		std::vector<const Key*> 							 arr{};

		std::unordered_map<Key, Value> 	keys{};
		Compare const comp{};
};

template<class Key, class Value, class Compare>
constexpr Heap<Key, Value, Compare>::Heap(size_t const& size)
{
	indexes.reserve(size);
	arr.reserve(size);
	keys.reserve(size);
}

template<class Key, class Value, class Compare>
[[nodiscard]] constexpr auto Heap<Key, Value, Compare>::top() const -> Key
{
	return *arr.at(0);
}

template<class Key, class Value, class Compare>
constexpr bool Heap<Key, Value, Compare>::isEmpty() const noexcept
{
	return arr.empty();
}

template<class Key, class Value, class Compare>
[[nodiscard]] constexpr size_t Heap<Key, Value, Compare>::left(size_t const& parent)
{
	size_t result = 2 * parent + 1;
	if (result <= parent) throw std::overflow_error("Parent's index is very high.");
	if (result >= arr.size()) return parent; else return result;
}

template<class Key, class Value, class Compare>
[[nodiscard]] constexpr size_t Heap<Key, Value, Compare>::right(size_t const& parent)
{
	size_t result = 2 * parent + 2;
	if (result <= parent) throw std::overflow_error("Parent's index is very high.");
	if (result >= arr.size()) return parent; else return result;
}

template<class Key, class Value, class Compare>
[[nodiscard]] constexpr size_t Heap<Key, Value, Compare>::parent(size_t const& child)
{
	if (child == 0) return 0; else return (child - 1) / 2;
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::insert(std::pair<Key, Value> const& node)
{
	auto const iter = keys.insert(node);
	if (iter.second) {
		insertLogic(iter.first->first);
	}
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::insert(std::pair<Key, Value>&& node)
{
	auto const iter = keys.insert(node);
	if (iter.second) {
		insertLogic(iter.first->first);
	}
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::insertLogic(Key const& key)
{
	indexes.insert(std::make_pair(&key, arr.size()));
	arr.emplace_back(&key);
	heapifyUp(arr.size() - 1);
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::heapifyUp(size_t const& index)
{
	size_t parentIndex = parent(index);
	if (!comp(keys[*arr[index]], keys[*arr[parentIndex]])) return;

	swapKeys(index, parentIndex);
	heapifyUp(parentIndex);
}

template<class Key, class Value, class Compare>
constexpr Key Heap<Key, Value, Compare>::popTop()
{
	auto result = keys.extract(*arr.at(0));
	swapKeys(0, arr.size() - 1);
	indexes.erase(arr.at(0));
	arr.pop_back();
	heapifyDown(0);
	return result.key();
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::swapKeys(size_t const a, size_t const b)
{
	std::swap(arr[a], arr[b]);
	std::swap(indexes[arr[a]], indexes[arr[b]]);
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::heapifyDown(size_t const& index)
{
	size_t childIndex = comp(keys[*arr[left(index)]], keys[*arr[right(index)]]) ? left(index) : right(index);
	if (!comp(keys[*arr[childIndex]], keys[*arr[index]])) return;

	swapKeys(index, childIndex);
	heapifyDown(childIndex);
}

template<class Key, class Value, class Compare>
constexpr void Heap<Key, Value, Compare>::resetValue(Key const& key, Value const& value)
{
	keys[key] = value;
	heapifyDown(indexes[&keys[key]]);
	heapifyUp(indexes[&keys[key]]);
}

#endif // MYHEAP_HPP