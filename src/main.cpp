#include <iostream>
#include <functional>
#include "MyHeap.hpp"

using namespace std;

int main()
{
	Heap<int, int> heap(6);
	auto myPair = make_pair(13, 13);
	heap.insert(myPair);
	heap.insert(make_pair(2, 10));
	heap.insert(make_pair(1, 20));
	heap.insert(make_pair(3, 2));
	heap.insert(make_pair(-9, 1230));
	heap.insert(make_pair(123, -98));
	heap.insert(make_pair(6, 23));

	cout << "Top is: " << heap.top() << '\n';

	heap.resetValue(1, -200);
	cout << "Top is: " << heap.top() << '\n';
	

	while (!heap.isEmpty()) {
		cout << "Top is: " << heap.popTop() << '\n';	
	}
	
	
  return 0;
}
